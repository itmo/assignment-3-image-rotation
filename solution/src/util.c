#include "statuses.h"
#include <stdint.h>

int64_t getPadding(const int64_t width) {
    return (4 - (width * 3) % 4) % 4;
}

const char * getStatusMsg(enum status st) {
    switch (st)
    {
    case OK:
        return "ВСЕ ОК";
    case FILE_NOT_FOUND:
        return "Файл не найден";
    case BITRATE_ERROR:
        return "Проблема с битностью входного изображения";
    case ENDIAN_ERROR:
        return "Входное изображение не поддерживается из-за порядка записи байтов";
    case FILE_SIZE_ERROR:
        return "В файле даже нет заголовка с метаданными!";
    case WRITE_ERROR:
        return "Проблема с записью в выходной файл";
    case NOT_ENOUGH_ARGUMENTS:
        return "Недостаточно аргументов";
    default:
        return "КАКАЯ-ТО ОШИБКА((";
    }
}
