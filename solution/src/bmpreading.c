#include "statuses.h"
#include "bmp.h"
#include "image.h"
#include "pixel.h"
#include "util.h"
#include <stdio.h>

enum status readPixels(FILE* in, struct pixel * pixels, int64_t width, int64_t height) {
    const int64_t padding = getPadding(width);
    char paddingReading[padding];
    size_t index;
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            index = i * width + j;
            fread(pixels + index, 1, 3, in);
        }
        fread(paddingReading, 1, padding, in);
    }
    return OK;
}

enum status handleHeaderErrors(const struct bmp_header header) {
    if (header.biBitCount != 24) {
        return BITRATE_ERROR;
    }
    if (header.bfType != 0x4D42) {
        return ENDIAN_ERROR;
    }
    if (header.bfileSize <= 54) {
        return FILE_SIZE_ERROR;
    }
    return OK;
}

enum status readBmp(FILE* in, struct bmp_header * header) {
    size_t read = fread(header, 1, 54, in);
    if (read != 54) {
        return READ_ERROR;
    }
    enum status headerStatus = handleHeaderErrors(*header);
    if (!headerStatus) {
        return headerStatus;
    }
    return OK;
} 

enum status fromBmp(FILE* in, struct image* img) {
    if (!in) {
        return FILE_NOT_FOUND;
    }

    struct bmp_header header = {0};
    enum status readBmpStatus = readBmp(in, &header);
    if (readBmpStatus) {
        return readBmpStatus;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;

    struct pixel * pixels = createTemplate(img->width, img->height);
    if (pixels == NULL) {
        return MEMALLOC_ERROR;
    }
    readPixels(in, pixels, img->width, img->height);

    destroyPixels(img->data);
    img->data = pixels;

    return OK;
}

enum status fromNamedBmp(const char* name, struct image* img) {
    FILE * in = fopen(name, "r");
    if (!name) {
        return FILE_NOT_FOUND;
    }
    enum status fromBmpStatus = fromBmp(in, img);
    fclose(in);
    return fromBmpStatus;
}
