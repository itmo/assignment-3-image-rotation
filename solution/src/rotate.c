#include "image.h"
#include "pixel.h"
#include <stddef.h>
#include <stdint.h>

struct image rotate(struct image const source) {
    struct image new = {0};
    new.width = source.height;
    new.height = source.width;
    new.data = createTemplate(new.width, new.height);
    size_t indexSource;
    size_t indexNew;
    for (size_t i = 0; i < source.height; ++i) {
        for(size_t j = 0; j < source.width; ++j) {
            indexSource = i * source.width + j;
            indexNew = source.height * j + source.height - i - 1;
            *(new.data + indexNew) = *(source.data + indexSource);
        }
    }

    return new;
}
