#include "statuses.h"
#include "bmp.h"
#include "image.h"
#include "pixel.h"
#include "util.h"
#include <stdint.h>
#include <stdio.h>

enum status writeHeader(FILE* out, struct bmp_header* header) {
    size_t written = fwrite(header, 1, 54, out);
    if (written != 54) {
        return WRITE_ERROR;
    }
    return OK;
}

enum status writePixels(FILE* out, struct pixel * pixels, int64_t width, int64_t height) {
    const int64_t padding = getPadding(width);
    const char paddingBytes[4] = {0};
    size_t index;
    size_t written;
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            index = i * width + j;
            written = fwrite(pixels + index, 1, 3, out);
            if (written != 3) {
                return WRITE_ERROR;
            }
        }
        written = fwrite(paddingBytes, 1, padding, out);
        if (written != padding) {
            return WRITE_ERROR;
        }
    }

    return OK;
}

enum status toBmp(FILE* out, struct image const* img) {
    if (!out) {
        return FILE_NOT_FOUND;
    }

    struct bmp_header header = createHeader(img->width, img->height);
    enum status writeHeaderStatus = writeHeader(out, &header);
    if (writeHeaderStatus) {
        return writeHeaderStatus;
    }

    enum status writePixelsStatus = writePixels(out, img->data, img->width, img->height);

    return writePixelsStatus;
}

enum status toNamedBmp(const char* name, struct image const* img) {
    FILE * out = fopen(name, "w");
    if (!out) {
        return FILE_NOT_FOUND;
    }
    enum status toBmpStatus = toBmp(out, img);
    fclose(out);
    return toBmpStatus;
}
