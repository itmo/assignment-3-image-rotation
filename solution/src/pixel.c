#include <malloc.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct pixel* createTemplate(const uint32_t width, const uint32_t height) {
    struct pixel * pixels = malloc(width * height * 3);
    return pixels;
}

void destroyPixels(struct pixel* pixels) {
    free(pixels);
}
