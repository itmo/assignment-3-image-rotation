#include "bmpreading.h"
#include "bmpwriting.h"
#include "image.h"
#include "rotate.h"
#include "statuses.h"
#include "util.h"
#include <inttypes.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc < 3) {
        printf("%s", getStatusMsg(NOT_ENOUGH_ARGUMENTS));
        return 0;
    }
    struct image img = {0};
    enum status readBmpStatus = fromNamedBmp(argv[1], &img);
    if (readBmpStatus) {
        printf("%s", getStatusMsg(readBmpStatus));
        destroyPixels(img.data);
        return 0;
    }

    struct image rotated = rotate(img);

    enum status writeBmpStatus = toNamedBmp(argv[2], &rotated);
    if (writeBmpStatus) {
        printf("%s", getStatusMsg(writeBmpStatus));
    }
    destroyPixels(img.data);
    destroyPixels(rotated.data);

    return 0;
}
