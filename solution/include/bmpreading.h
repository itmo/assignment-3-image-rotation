#pragma once
#include "image.h"
#include "statuses.h"
#include <stdio.h>

enum status fromNamedBmp(const char* name, struct image* img);
