#pragma once

enum bmpstdvalue {
    BF_TYPE = 0x4D42,
    BF_RESERVED = 0,
    B_OFF_BITS = 54,
    BI_SIZE = 40,
    BI_PLANES = 1,
    BI_BIT_COUNT = 24,
    BI_COMPRESSION = 0,
    BI_X_PELS_PER_METER = 2834,
    BI_Y_PELS_PER_METER = 2834,
    BI_CLR_USED = 0,
    BI_CLR_IMPORTANT = 0
};
