#pragma once
#include "pixel.h"
#include <stdint.h>

struct image {
  int64_t width, height;
  struct pixel *data;
};
