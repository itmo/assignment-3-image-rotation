#pragma once
#include <stdint.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType; // содержит инфу о том, le или be тут данные, то есть значение либо 4D42 - le, либо 424D - be. x86 - le
    uint32_t bfileSize; // размер файла (вместе с заголовком)
    uint32_t bfReserved; // по нулям
    uint32_t bOffBits; // сдвиг от заголовка, там и начинаются данные
    uint32_t biSize; // информация о версии. 40 - 3 версия bmp
    uint32_t biWidth; // ширина изображения
    uint32_t biHeight; // высота изображения
    uint16_t biPlanes; // "В BMP допустимо только значение 1. Это поле используется в значках и курсорах Windows" - вики
    uint16_t biBitCount; // количество бит на пиксель. 24 в данном случае
    uint32_t biCompression; // способ хранения данных изображения. 0 - просто двумерный массив, то есть наш случай
    uint32_t biSizeImage; // размер файла (без заголовка). "Может быть 0, если хранение осуществляется двумерным массивом." - вики. = (width * 3 + padding) * height
    uint32_t biXPelsPerMeter; // кол-в пикселей на метр. использовать 2834 в обоих случаях (почему? потому что)
    uint32_t biYPelsPerMeter; 
    uint32_t biClrUsed; // размер таблицы цветов. нужно, если изображение <= 8 бит или если заголовок не CORE-версии (древней). главное, что в данном случае по нулям
    uint32_t biClrImportant; // "Количество ячеек от начала таблицы цветов до последней используемой (включая её саму)" - вики
};

struct bmp_header createHeader(int64_t width, int64_t height);
