#pragma once
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct pixel* createTemplate(const uint32_t width, const uint32_t height);

void destroyPixels(struct pixel* pixels);
