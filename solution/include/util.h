#pragma once
#include "statuses.h"
#include <stdint.h>

int64_t getPadding(const int64_t width);

const char * getStatusMsg(enum status st);
